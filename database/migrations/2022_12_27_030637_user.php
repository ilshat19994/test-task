<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id')->default(DB::raw('gen_random_uuid()::uuid'))->primary();
            $table->string('email');
            $table->boolean('email_confirmed')->default(false);
            $table->bigInteger('phone')->nullable();
            $table->boolean('phone_confirmed')->default(false);
            $table->string('password_hash');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
