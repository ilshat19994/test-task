# Ibox

## Запуск:
- Необходимо скопировать и переименовать [.env.example](.env.example) в  [.env](.env)
- `make run`

## При первом запуске необходимо создать пользователя:
- `docker-compose run --rm php php artisan create:user`
