<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface BaseRepositoryContract
{

    /**
     * @return mixed
     */
    public function getFinder(): mixed;

    /**
     * @param array $filter
     * @param array $with
     * @param array $column
     * @return Collection
     */
    public function list(array $filter, array $with = [], array $column = ['*']): Collection;

    /**
     * @param $id
     * @param bool $orFail
     * @param string $idKey
     * @param array $with
     * @return mixed
     */
    public function findById($id, bool $orFail = false, string $idKey = 'id', array $with = []): mixed;

    /**
     * @param array $filter
     * @param bool $orFail
     * @param array $with
     * @return mixed
     */
    public function findOne(array $filter, bool $orFail = false, array $with = []): mixed;

    /**
     * @param array|null $filter
     * @param int|null $perPage
     * @param int|null $currentPage
     * @param array $with
     * @param array $columns
     * @return LengthAwarePaginator
     */
    public function findWithPaginator(?array $filter, ?int $perPage, ?int $currentPage, array $with = [], array $columns = ['*']): LengthAwarePaginator;
}
