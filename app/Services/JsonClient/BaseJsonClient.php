<?php

namespace App\Services\JsonClient;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use JsonException;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Throwable;

abstract class BaseJsonClient
{
    private LogStage $logStage;

    public function __construct()
    {
        $this->logStage = new LogStage();
    }

    /**
     * @return LogStage
     */
    public function logStage(): LogStage
    {
        return $this->logStage;
    }

    /**
     * @param  PendingRequest  $client
     * @param  string  $method
     * @param  string  $url
     * @param  array  $params
     * @param  array  $headers
     * @return array
     * @throws JsonException
     */
    public function request(
        PendingRequest $client,
        string         $method,
        string         $url,
        array          $params = [],
        array          $headers = []
    ): array
    {
        $this->logStage
            ->setUrl($url)
            ->setTypeRequest($method)
            ->setRequestParams($params);

        if ($headers) {
            $client->withHeaders($headers);
            $this->logStage->setHeaders($headers);
        }

        try {
            $response = $client->$method($url, $params);
            assert($response instanceof Response);

            $this->logStage
                ->setResponseBody($response->body())
                ->setResponseCode($response->status())
                ->setResponseHeaders($response->headers());

            $body = $response->throw()->body();

            [$jsonData, $isValidJson] = $this->parseJson($body);

            $this->logStage->setResponseJson($jsonData)->setIsValidJson($isValidJson);
        } catch (ConnectionException) {
            $this->logStage
                ->setError('Сетевая ошибка или таймаут соединения')
                ->setIsValidJson(false)
                ->setResponseCode(ResponseCode::HTTP_SERVICE_UNAVAILABLE);

            $jsonData = [];
        } catch (RequestException $e) {
            $body = $e->response->body();

            [$jsonData, $isValidJson] = $this->parseJson($body);

            $this->logStage
                ->setResponseBody($body)
                ->setResponseCode($e->getCode())
                ->setError($e->getMessage())
                ->setErrorTitle($e->response->toPsrResponse()->getReasonPhrase())
                ->setResponseJson($jsonData)
                ->setIsValidJson($isValidJson);
        } catch (Throwable $e) {
            $this->logStage
                ->setError($e->getMessage())
                ->setIsValidJson(false)
                ->setResponseCode(ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
            $jsonData = [];
        }

        return $jsonData;
    }

    /**
     * @param  string  $data
     * @return array
     */
    private function parseJson(string $data): array
    {
        $result = json_decode($data, true);
        $isSuccess = true;

        if (json_last_error() !== JSON_ERROR_NONE) {
            $result = [];
            $isSuccess = false;
        }

        return [$result, $isSuccess];
    }
}
