<?php

namespace App\Services\JsonClient;

/**
 * Класс для хранения данных о запросе.
 */
class LogStage
{
    private string $url;
    private string $typeRequest;
    private array $requestParams = [];
    private array $headers = [];
    private string $description = 'single';
    private string $responseBody = '';
    private array $responseJson = [];
    private bool $isValidJson = false;
    private int $responseCode;
    private array $responseHeaders = [];
    private string $error = '';
    private string $errorTitle;

    /**
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return "[{$this->typeRequest}] {$this->url}";
    }

    /**
     * @param string $url
     * @return LogStage
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getTypeRequest(): string
    {
        return $this->typeRequest;
    }

    /**
     * @param string $typeRequest
     * @return LogStage
     */
    public function setTypeRequest(string $typeRequest): static
    {
        $this->typeRequest = $typeRequest;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequestParams(): array
    {
        return $this->requestParams;
    }

    /**
     * @param array $requestParams
     * @return LogStage
     */
    public function setRequestParams(array $requestParams): static
    {
        $this->requestParams = $requestParams;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return LogStage
     */
    public function setHeaders(array $headers): static
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return LogStage
     */
    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getResponseBody(): string
    {
        return $this->responseBody;
    }

    /**
     * @param string $responseBody
     * @return LogStage
     */
    public function setResponseBody(string $responseBody): static
    {
        $this->responseBody = $responseBody;

        return $this;
    }

    /**
     * @return array
     */
    public function getResponseJson(): array
    {
        return $this->responseJson;
    }

    /**
     * @param $data
     * @return LogStage
     */
    public function setResponseJson($data): static
    {
        if (is_array($data)) {
            $this->responseJson = $data;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isValidJson(): bool
    {
        return $this->isValidJson;
    }

    /**
     * @param bool $isValidJson
     * @return LogStage
     */
    public function setIsValidJson(bool $isValidJson): static
    {
        $this->isValidJson = $isValidJson;

        return $this;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $error
     * @return LogStage
     */
    public function setError(string $error): static
    {
        $this->error = str_replace(PHP_EOL, '', $error);

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorTitle(): string
    {
        return $this->errorTitle;
    }

    /**
     * @param string $errorTitle
     * @return LogStage
     */
    public function setErrorTitle(string $errorTitle): static
    {
        $this->errorTitle = $errorTitle;

        return $this;
    }

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * @param int $responseCode
     * @return LogStage
     */
    public function setResponseCode(int $responseCode): static
    {
        $this->responseCode = $responseCode;

        return $this;
    }

    /**
     * @return array
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    /**
     * @param array $responseHeaders
     * @return LogStage
     */
    public function setResponseHeaders(array $responseHeaders): static
    {
        $this->responseHeaders = $responseHeaders;

        return $this;
    }
}
