<?php

namespace App\Exceptions;

use App\Exceptions\Enum\ExceptionEnum;
use Exception;

class BaseException extends Exception
{
    public function __construct(ExceptionEnum $enum, Exception $previous = null)
    {
        parent::__construct($enum->getMessage(), $enum->value, $previous);
    }
}
