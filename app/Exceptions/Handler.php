<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        $this->renderable(function (ValidationException $e) {
            return response()->json([
                'data'         => null,
                'error'        => $e->errors(),
                'status'       => 422,
                'status_check' => false
            ], 422);
        });

        $this->renderable(function (Throwable $e) {
            return response()->json([
                'data'   => $e->getMessage(),
                'status' => $e->getCode(),
                'error'  => "Error",
            ], 500);
        });

        $this->renderable(function (BaseException $e) {
            return response()->json([
                'data'         => '',
                'error'        => $e->getMessage(),
                'status'       => $e->getCode(),
                'status_check' => false
            ], 203);
        });
    }
}
