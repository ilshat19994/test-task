<?php

namespace App\Exceptions\Enum;

enum ExceptionEnum: int
{
    case POST_SAVE_ERROR = 301;
    case MOVIE_SAVE_ERROR = 302;

    public function getMessage(): string
    {
        return match ($this) {
            self::POST_SAVE_ERROR => 'Ошибка при сохранение поста.',
            self::MOVIE_SAVE_ERROR => 'Ошибка при сохранение информации о фильме.',
        };
    }
}
