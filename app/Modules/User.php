<?php

namespace App\Modules;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->table = 'users';
        parent::__construct($attributes);
    }

    use HasFactory;

    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'email',
        'email_confirmed',
        'phone',
        'phone_confirmed',
        'password_hash',
        'created_at',
        'updated_at',
    ];
}
