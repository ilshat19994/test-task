<?php

namespace App\Modules\Post;

use App\Modules\Post\Contracts\PostRepositoryContract;
use App\Modules\Post\Contracts\PostServiceContract;
use App\Modules\Post\Repositories\PostRepository;
use App\Modules\Post\Services\PostService;
use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(PostServiceContract::class, PostService::class);
        $this->app->bind(PostRepositoryContract::class, PostRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }
}
