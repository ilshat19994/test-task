<?php

namespace App\Modules\Post\Services;

use App\Modules\Post\Contracts\PostRepositoryContract;
use App\Modules\Post\Contracts\PostServiceContract;
use App\Modules\Post\DTOs\CreatePostDto;
use App\Modules\Post\DTOs\IndexPostDto;
use App\Modules\Post\DTOs\UpdatePostDto;
use App\Modules\Post\Models\Post;
use Illuminate\Pagination\LengthAwarePaginator;

class PostService implements PostServiceContract
{
    /**
     * @param  PostRepositoryContract  $postRepository
     */
    public function __construct(private readonly PostRepositoryContract $postRepository)
    {
    }

    /**
     * @param  CreatePostDto  $postDto
     * @return Post
     */
    public function store(CreatePostDto $postDto): Post
    {
        return $this->postRepository->store($postDto);
    }

    /**
     * @param  UpdatePostDto  $postDto
     * @param  string  $postId
     * @return Post
     */
    public function update(UpdatePostDto $postDto, string $postId): Post
    {
        return $this->postRepository->update($postDto, $postId);
    }

    /**
     * @param  string  $postId
     * @return Post
     */
    public function show(string $postId): Post
    {
        return $this->postRepository->show($postId);
    }

    /**
     * @param  IndexPostDto  $indexPostDto
     * @return LengthAwarePaginator
     */
    public function index(IndexPostDto $indexPostDto): LengthAwarePaginator
    {
        return $this->postRepository->index($indexPostDto);
    }
}