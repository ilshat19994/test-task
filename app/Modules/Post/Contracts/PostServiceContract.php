<?php

namespace App\Modules\Post\Contracts;

use App\Modules\Post\DTOs\CreatePostDto;
use App\Modules\Post\DTOs\IndexPostDto;
use App\Modules\Post\DTOs\UpdatePostDto;
use App\Modules\Post\Models\Post;
use Illuminate\Pagination\LengthAwarePaginator;

interface PostServiceContract
{
    /**
     * @param  CreatePostDto  $postDto
     * @return Post
     */
    public function store(CreatePostDto $postDto): Post;

    /**
     * @param  UpdatePostDto  $postDto
     * @param  string  $postId
     * @return Post
     */
    public function update(UpdatePostDto $postDto, string $postId): Post;

    /**
     * @param  string  $postId
     * @return Post
     */
    public function show(string $postId): Post;

    /**
     * @param  IndexPostDto  $indexPostDto
     * @return LengthAwarePaginator
     */
    public function index(IndexPostDto $indexPostDto): LengthAwarePaginator;
}