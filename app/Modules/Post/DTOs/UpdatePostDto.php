<?php

namespace App\Modules\Post\DTOs;

class UpdatePostDto
{
    /**
     * @param  string  $title
     * @param  string  $description
     */
    public function __construct(
        public readonly string $title,
        public readonly string $description,
    ) {
    }
}