<?php

namespace App\Modules\Post\DTOs;

class IndexPostDto
{
    /**
     * @param  array  $filter
     * @param  int|null  $perPage
     * @param  int|null  $page
     */
    public function __construct(
        public readonly array $filter,
        public readonly ?int $perPage,
        public readonly ?int $page,
    ) {
    }
}