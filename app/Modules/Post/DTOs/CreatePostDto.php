<?php

namespace App\Modules\Post\DTOs;

class CreatePostDto
{
    /**
     * @param  string  $title
     * @param  string  $description
     * @param  string  $createdBy
     */
    public function __construct(
        public readonly string $title,
        public readonly string $description,
        public readonly string $createdBy
    ) {
    }
}