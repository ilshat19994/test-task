<?php

namespace App\Modules\Post\Http\Resources;

use App\Modules\Post\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Post $this */
        return [
            'id'          => $this->resource->id,
            'title'       => $this->resource->title,
            'description' => $this->resource->description,
            'created_by'  => $this->resource->created_by,
            'created_at'  => $this->resource->created_at,
        ];
    }
}
