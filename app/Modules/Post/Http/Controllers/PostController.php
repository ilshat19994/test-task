<?php

namespace App\Modules\Post\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Post\Contracts\PostServiceContract;
use App\Modules\Post\Http\Requests\CreatePostRequest;
use App\Modules\Post\Http\Requests\IndexPostRequest;
use App\Modules\Post\Http\Requests\UpdatePostRequest;
use App\Modules\Post\Http\Resources\PostResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PostController extends Controller
{
    public function __construct(private readonly PostServiceContract $postService)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @param  IndexPostRequest  $request
     * @return JsonResource
     */
    public function index(IndexPostRequest $request): JsonResource
    {
        $posts = $this->postService->index($request->getDto());

        return $this->successResponse(PostResource::collection($posts));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePostRequest  $request
     * @return JsonResource
     */
    public function store(CreatePostRequest $request): JsonResource
    {
        $post = $this->postService->store($request->getDto());

        return $this->successResponse(new PostResource($post));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return JsonResource
     */
    public function show(string $id): JsonResource
    {
        $post = $this->postService->show($id);

        return $this->successResponse(new PostResource($post));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePostRequest  $request
     * @param  string  $id
     * @return JsonResource
     */
    public function update(UpdatePostRequest $request, string $id): JsonResource
    {
        $post = $this->postService->update($request->getDto(), $id);

        return $this->successResponse(new PostResource($post));
    }
}
