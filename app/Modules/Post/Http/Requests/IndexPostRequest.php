<?php

namespace App\Modules\Post\Http\Requests;

use App\Modules\Post\DTOs\CreatePostDto;
use App\Modules\Post\DTOs\IndexPostDto;
use App\Modules\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class IndexPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'       => ['sometimes', 'string'],
            'description' => ['sometimes', 'string'],
            'page'        => ['sometimes', 'int'],
            'per_page'    => ['sometimes', 'int'],
            'order'       => ['sometimes', 'string'],
        ];
    }

    public function getDto(): IndexPostDto
    {
        return new IndexPostDto(
            filter: Arr::except($this->all(), ['page', 'per_page']),
            perPage: $this->get('per_page'),
            page: $this->get('page'),
        );
    }
}
