<?php

namespace App\Modules\Post\Http\Requests;

use App\Modules\Post\DTOs\CreatePostDto;
use App\Modules\Post\DTOs\UpdatePostDto;
use App\Modules\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'       => ['required', 'string'],
            'description' => ['required', 'string'],
        ];
    }

    public function getDto(): UpdatePostDto
    {
        return new UpdatePostDto(
            title: $this->get('title'),
            description: $this->get('description'),
        );
    }
}
