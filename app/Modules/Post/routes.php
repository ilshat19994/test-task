<?php

use App\Modules\Post\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => 'api/v1/',
    'middleware' => ['api']
], static function () {
    Route::apiResource('post', PostController::class);
});
