<?php

namespace App\Modules\Post\Finders;

use App\Modules\Post\Models\Post;
use App\Support\BaseFinder;
use Illuminate\Database\Eloquent\Builder;

class PostFinder extends BaseFinder
{
    protected array $allowOrders = ['created_at'];
    protected array $defaultOrders = ['-created_at'];

    /**
     * @return Builder
     */
    public function makeBuilder(): Builder
    {
        return Post::query();
    }

    /**
     * @param  string  $value
     * @return void
     */
    public function description(string $value): void
    {
        $this->builder->where('description', 'like', '%'.$value.'%');
    }

    /**
     * @param  string  $value
     * @return void
     */
    public function title(string $value): void
    {
        $this->builder->where('title', 'like', '%'.$value.'%');
    }
}