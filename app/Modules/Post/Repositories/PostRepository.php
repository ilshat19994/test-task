<?php

namespace App\Modules\Post\Repositories;

use App\Exceptions\Enum\ExceptionEnum;
use App\Modules\Post\Contracts\PostRepositoryContract;
use App\Modules\Post\DTOs\CreatePostDto;
use App\Modules\Post\DTOs\IndexPostDto;
use App\Modules\Post\DTOs\UpdatePostDto;
use App\Modules\Post\Exceptions\SavePostException;
use App\Modules\Post\Finders\PostFinder;
use App\Modules\Post\Models\Post;
use App\Support\BaseRepository;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use Throwable;

class PostRepository extends BaseRepository implements PostRepositoryContract
{
    protected string $finderClass = PostFinder::class;

    /**
     * @param  CreatePostDto  $postDto
     * @return Post
     * @throws Throwable
     */
    public function store(CreatePostDto $postDto): Post
    {
        $post              = new Post();
        $post->title       = $postDto->title;
        $post->description = $postDto->description;
        $post->created_by  = $postDto->createdBy;

        throw_unless($post->save(), new SavePostException(ExceptionEnum::POST_SAVE_ERROR));

        return $post;
    }

    /**
     * @param  UpdatePostDto  $postDto
     * @param  string  $postId
     * @return Post
     * @throws Throwable
     */
    public function update(UpdatePostDto $postDto, string $postId): Post
    {
        $post              = $this->findById(id: $postId, orFail: true);
        $post->title       = $postDto->title;
        $post->description = $postDto->description;

        throw_unless($post->save(), new SavePostException(ExceptionEnum::POST_SAVE_ERROR));

        return $post;
    }

    /**
     * @param  string  $postId
     * @return Post
     * @throws Exception
     */
    public function show(string $postId): Post
    {
        return $this->findById(id: $postId, orFail: true);
    }

    /**
     * @param  IndexPostDto  $indexPostDto
     * @return LengthAwarePaginator
     * @throws Exception
     */
    public function index(IndexPostDto $indexPostDto): LengthAwarePaginator
    {
        return $this->findWithPaginator(
            filter: $indexPostDto->filter,
            perPage: $indexPostDto->perPage,
            currentPage: $indexPostDto->page
        );
    }
}