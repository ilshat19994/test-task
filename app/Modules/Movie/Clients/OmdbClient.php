<?php

namespace App\Modules\Movie\Clients;

use App\Modules\Movie\Configs\OmdbConfig;
use App\Services\JsonClient\BaseJsonClient;
use App\Support\Traits\LogTrait;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use JsonException;

class OmdbClient extends BaseJsonClient
{
    use LogTrait;

    /**
     * @param  array  $info
     * @return array
     * @throws JsonException
     */
    public function findMovie(array $info): array
    {
        $this->logStage()->setDescription('Получение информации о фильме.');
        $response = $this->request($this->getClient(), 'GET', OmdbConfig::findMovieUrl(), $info);
        $this->log($this->logStage(), [], OmdbConfig::logChannel());

        return $response;
    }

    /**
     * @return PendingRequest
     */
    private function getClient(): PendingRequest
    {
        return Http::asJson()
            ->acceptJson()
            ->timeout(OmdbConfig::timeOut());
    }
}