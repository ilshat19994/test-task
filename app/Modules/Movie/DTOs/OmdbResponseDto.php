<?php

namespace App\Modules\Movie\DTOs;

class OmdbResponseDto
{
    /**
     * @param  string  $title
     * @param  string  $year
     * @param  string  $rated
     * @param  string  $released
     * @param  string  $runtime
     * @param  string  $genre
     */
    public function __construct(
        public string $title,
        public string $year,
        public string $rated,
        public string $released,
        public string $runtime,
        public string $genre
    ) {
    }

    /**
     * @param  array  $response
     * @return static
     */
    public static function createFromResponse(array $response): self
    {
        return new self(
            title: $response['Title'],
            year: $response['Year'],
            rated: $response['Rated'],
            released: $response['Released'],
            runtime: $response['Runtime'],
            genre: $response['Genre'],
        );
    }
}