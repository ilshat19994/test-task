<?php

namespace App\Modules\Movie\Contracts;

use App\Modules\Movie\DTOs\OmdbResponseDto;
use App\Modules\Movie\Models\Movie;

interface MovieRepositoryContract
{
    /**
     * @param  OmdbResponseDto  $dto
     * @return Movie
     */
    public function create(OmdbResponseDto $dto): Movie;
}