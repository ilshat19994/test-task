<?php

namespace App\Modules\Movie\Contracts;

use App\Modules\Movie\DTOs\OmdbResponseDto;

interface MovieServiceContract
{
    /**
     * @param  string  $title
     * @return OmdbResponseDto
     */
    public function find(string $title): OmdbResponseDto;
}