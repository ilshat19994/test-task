<?php

use App\Modules\Movie\Http\Controllers\MovieController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => 'api/v1/',
    'middleware' => ['api']
], static function () {
    Route::get('movie', [MovieController::class, 'find']);
});
