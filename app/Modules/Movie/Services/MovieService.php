<?php

namespace App\Modules\Movie\Services;

use App\Modules\Movie\Clients\OmdbClient;
use App\Modules\Movie\Configs\OmdbConfig;
use App\Modules\Movie\Contracts\MovieRepositoryContract;
use App\Modules\Movie\Contracts\MovieServiceContract;
use App\Modules\Movie\DTOs\OmdbResponseDto;
use App\Modules\Movie\Exceptions\OmdbException;
use JsonException;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Cache;

class MovieService implements MovieServiceContract
{
    /**
     * @param  OmdbClient  $omdbClient
     * @param  MovieRepositoryContract  $movieRepository
     */
    public function __construct(
        private readonly OmdbClient $omdbClient,
        private readonly MovieRepositoryContract $movieRepository
    ) {
    }

    /**
     * @param  string  $title
     * @return OmdbResponseDto
     * @throws OmdbException
     * @throws JsonException
     */
    public function find(string $title): OmdbResponseDto
    {
        if ($response = Cache::get($title)) {
            return $response;
        }

        // Если это было бы сервисом смс рассылки, либо еще чего важного. То можно реализовать паттерн выклчателя.
        $info     = $this->initData($title);
        $response = $this->omdbClient->findMovie($info);
        $this->checkSuccessResponse($response);

        $omdbDto = OmdbResponseDto::createFromResponse($response);
        Cache::put($title, $omdbDto, OmdbConfig::cacheTtl());
        $this->movieRepository->create($omdbDto);

        return $omdbDto;
    }

    /**
     * @param  array  $response
     * @return void
     * @throws OmdbException
     */
    private function checkSuccessResponse(array $response): void
    {
        if (
            !isset($response['Year']) ||
            $this->omdbClient->logStage()->getResponseCode() !== ResponseCode::HTTP_OK
        ) {
            throw new OmdbException(
                message: 'Ошибка при получение фильма.'
            );
        }
    }

    /**
     * @param  string  $title
     * @return array
     */
    private function initData(string $title): array
    {
        return [
            'apikey' => OmdbConfig::apiKey(),
            't'      => $title
        ];
    }
}