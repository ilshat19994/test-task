<?php

namespace App\Modules\Movie;

use App\Modules\Movie\Contracts\MovieRepositoryContract;
use App\Modules\Movie\Contracts\MovieServiceContract;
use App\Modules\Movie\Repositories\MovieRepository;
use App\Modules\Movie\Services\MovieService;
use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(MovieServiceContract::class, MovieService::class);
        $this->app->bind(MovieRepositoryContract::class, MovieRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }
}
