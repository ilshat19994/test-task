<?php

namespace App\Modules\Movie\Configs;

class OmdbConfig
{
    /**
     * @return string
     */
    public static function findMovieUrl(): string
    {
        return 'http://www.omdbapi.com';
    }

    /**
     * @return int
     */
    public static function timeOut(): int
    {
        return 20;
    }

    /**
     * @return string
     */
    public static function apiKey(): string
    {
        return config('services.omdb.api_key');
    }

    /**
     * @return string
     */
    public static function logChannel(): string
    {
        return 'omdb';
    }

    /**
     * @return int
     */
    public static function cacheTtl(): int
    {
        return 240;
    }
}