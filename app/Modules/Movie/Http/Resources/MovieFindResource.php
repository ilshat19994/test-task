<?php

namespace App\Modules\Movie\Http\Resources;

use App\Modules\Movie\DTOs\OmdbResponseDto;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieFindResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $omdbResponse = $this->resource;
        assert($omdbResponse instanceof OmdbResponseDto);

        return [
            'title'    => $omdbResponse->title,
            'year'     => $omdbResponse->year,
            'rated'    => $omdbResponse->rated,
            'released' => $omdbResponse->released,
            'runtime'  => $omdbResponse->runtime,
            'genre'    => $omdbResponse->genre,
        ];
    }
}