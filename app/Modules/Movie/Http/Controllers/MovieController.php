<?php

namespace App\Modules\Movie\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Movie\Contracts\MovieServiceContract;
use App\Modules\Movie\Http\Requests\FindMovieRequest;
use App\Modules\Movie\Http\Resources\MovieFindResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieController extends Controller
{
    public function __construct(private readonly MovieServiceContract $movieService)
    {
    }

    /**
     * @param  FindMovieRequest  $request
     * @return JsonResource
     */
    public function find(FindMovieRequest $request): JsonResource
    {
        $info = $this->movieService->find($request->get('title'));

        return $this->successResponse(new MovieFindResource($info));
    }
}
