<?php

namespace App\Modules\Movie\Repositories;

use App\Exceptions\Enum\ExceptionEnum;
use App\Modules\Movie\Contracts\MovieRepositoryContract;
use App\Modules\Movie\DTOs\OmdbResponseDto;
use App\Modules\Movie\Exceptions\SaveMovieException;
use App\Modules\Movie\Models\Movie;
use Throwable;

class MovieRepository implements MovieRepositoryContract
{
    /**
     * @param  OmdbResponseDto  $dto
     * @return Movie
     * @throws Throwable
     */
    public function create(OmdbResponseDto $dto): Movie
    {
        $movie           = new Movie();
        $movie->title    = $dto->title;
        $movie->year     = $dto->year;
        $movie->rated    = $dto->rated;
        $movie->released = $dto->released;
        $movie->runtime  = $dto->runtime;
        $movie->genre    = $dto->genre;

        throw_unless($movie->save(), new SaveMovieException(ExceptionEnum::MOVIE_SAVE_ERROR));

        return $movie;
    }
}