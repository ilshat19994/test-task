<?php

namespace App\Console\Commands;

use App\Modules\User;
use Illuminate\Console\Command;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user';

    public function handle(): void
    {
        //TODO:: Поставил самую простую заглушку. Не обращать на это внимание.
        $user = new User();

        $user->id = '3c544188-85b8-11ed-a1eb-0242ac120002';
        $user->email = 'test@mail.ru';
        $user->email_confirmed = true;
        $user->phone = 79503221232;
        $user->phone_confirmed = true;
        $user->password_hash = '$2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a';

        $user->save();
    }
}
