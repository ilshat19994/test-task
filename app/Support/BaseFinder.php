<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

abstract class BaseFinder
{
    protected Builder $builder;

    /** список полей по которым можно сортировать */
    protected array $allowOrders = [];

    /** список полей по которым нужно сортировать по умолчанию */
    protected array $defaultOrders = [];

    /** маппинг полей для сортировки в виде ['rest-параметр' => 'поле в базе'] */
    protected array $mappingOrders = [];

    public function __construct()
    {
        $this->setBuilder($this->makeBuilder());
    }

    /**
     * @return mixed
     */
    abstract protected function makeBuilder();

    /**
     * @param $builder
     * @return $this
     */
    public function setBuilder($builder): static
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * @param  array  $filter
     * @param  string  $boolean
     * @return $this
     */
    public function apply(array $filter, string $boolean = 'and'): self
    {
        $orders = null;

        foreach ($filter as $key => $value) {
            $method = Str::camel($key);

            if (method_exists($this, $method) && !empty($value)) {
                $this->{$method}($value, $boolean);
            }

            if ($method === 'order') {
                $orders = $value;
            }
        }

        $this->order($orders);

        return $this;
    }

    /**
     * @param $columns
     * @return Collection|array
     */
    public function get($columns = ['*']): Collection|array
    {
        return $this->builder->get($columns);
    }

    /**
     * @return Model|$this|null
     */
    public function first(): Model|static|null
    {
        return $this->builder->first();
    }

    /**
     * @return Model|Builder
     */
    public function firstOrFail(): Model|Builder
    {
        return $this->builder->firstOrFail();
    }

    /**
     * @param $relations
     * @return $this
     */
    public function with($relations): self
    {
        $this->builder->with($relations);

        return $this;
    }

    /**
     * @param $id
     * @param $boolean
     * @return $this
     */
    public function id($id, $boolean): static
    {
        $id = is_array($id) ? $id : [$id];
        $this->builder->whereIn('id', $id, $boolean);

        return $this;
    }

    /**
     * @param  int  $perPage
     * @param  int  $currentPage
     * @param  array  $columns
     * @return LengthAwarePaginator
     */
    public function paginate(int $perPage, int $currentPage, array $columns = ['*']): LengthAwarePaginator
    {
        return $this->builder->paginate(
            perPage: $perPage,
            columns: $columns,
            page: $currentPage
        );
    }

    /**
     * @param  string|null  $orders
     * @return $this
     */
    public function order(?string $orders): self
    {
        isset($orders) ? $fields = array_filter(explode(',', $orders)) : $fields = $this->defaultOrders;

        foreach ($fields as $field) {
            $column    = ltrim($field, '-');
            $direction = Str::startsWith($field, '-') ? 'desc' : 'asc';

            if (in_array($column, $this->allowOrders)) {
                $queryColumn = $this->mappingOrders[$column] ?? $column;

                $this->builder->orderBy($queryColumn, $direction);
            }
        }

        return $this;
    }
}
