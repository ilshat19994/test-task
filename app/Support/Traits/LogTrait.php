<?php

namespace App\Support\Traits;

use App\Services\JsonClient\LogStage;
use Illuminate\Support\Facades\Log;

trait LogTrait
{
    /**
     * @param  LogStage  $log
     * @param  array  $params
     * @param  string  $channel
     * @return void
     */
    public function log(LogStage $log, array $params = [], string $channel = 'single'): void
    {
        $title = $log->getDescription();

        $printData = array_merge([
            'url'           => $log->getUrl(),
            'data'          => $log->getRequestParams(),
            'response'      => $log->isValidJson() ? $log->getResponseJson() : $log->getResponseBody(),
            'code'          => $log->getResponseCode(),
            'is_valid_json' => $log->isValidJson(),
            'error'         => $log->getError(),
        ], $params);

        Log::channel($channel)->info($title, $printData);
    }
}
