<?php

namespace App\Support;

use App\Contracts\BaseRepositoryContract;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements BaseRepositoryContract
{
    /**
     * Ссылка на класс или интерфейс finder'а
     *
     * @var string
     */
    protected string $finderClass;

    /**
     * @throws Exception
     */
    public function getFinder(): mixed
    {
        if (!$this->finderClass) {
            throw new Exception('Finder not defined');
        }

        return app($this->finderClass);
    }

    /**
     * @throws Exception
     */
    public function list(array $filter, array $with = [], array $column = ['*']): Collection
    {
        return $this->getFinder()->apply($filter)->with($with)->get($column);
    }

    /**
     * @throws Exception
     */
    public function findById($id, bool $orFail = false, string $idKey = 'id', array $with = []): mixed
    {
        return $this->findOne([$idKey => $id], $orFail, $with);
    }

    /**
     * @throws Exception
     */
    public function findOne(array $filter, bool $orFail = false, array $with = []): mixed
    {
        $query = $this->getFinder()->apply($filter)->with($with);

        return $orFail ? $query->firstOrFail() : $query->first();
    }

    /**
     * @throws Exception
     */
    public function findWithPaginator(
        ?array $filter,
        ?int   $perPage,
        ?int   $currentPage,
        array  $with = [],
        array  $columns = ['*']
    ): LengthAwarePaginator
    {
        $perPage = $perPage ? min($perPage, 50) : 20;
        $currentPage = $currentPage ?? 1;

        return $this
            ->getFinder()
            ->apply($filter)
            ->with($with)
            ->paginate($perPage, $currentPage, $columns);
    }
}
