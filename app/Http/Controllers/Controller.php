<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param  JsonResource  $resource
     * @return JsonResource
     */
    protected function successResponse(JsonResource $resource): JsonResource
    {
        return $resource->additional(
            [
                'status'       => 200,
                'status_check' => true
            ]
        );
    }
}
