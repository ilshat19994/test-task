<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class StripText
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $data = $request->all();

        $data = $this->getRequestParams($data);

        $request->merge($data);

        return $next($request);
    }

    private function getRequestParams(array $requests) : array
    {
        $result = [];

        foreach ($requests as $key => $request) {
            if(is_array($request)) {
                $result[$key] = $this->getRequestParams($request);
            } else {
                $result[$key] = htmlspecialchars($request);
            }
        }

        return $result;
    }
}
