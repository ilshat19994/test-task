<?php

return [
    'tds' => env('TDS_TABLE_NAME', 'tds'),
    'users' => env('USER_TABLE_NAME', 'users')
];
